package pl.softiq.db;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
public class Publisher {

    private int id;
    private String name;
    private String logoImage;

    private final Set<Book> books = new HashSet<>();

    public Publisher withBook(Book book){
        books.add(book);
        return this;
    }

}
