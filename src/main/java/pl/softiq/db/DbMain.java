package pl.softiq.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbMain {

    public static void main(String[] args) {
        System.out.println("Let's process relational data");

        try(Connection connection = getConnection()){
            System.out.println("successfully connected with DB.");

            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID, NAME, LOGOIMAGE FROM PUBLISHER");

            List<Publisher> publishers = new ArrayList<>();

            while(rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString("NAME");
                String logo = rs.getString("LOGOIMAGE");
                System.out.println("publisher: " + id + ", " + name + ", " + logo);
                publishers.add(new Publisher(id, name, logo));
            }


            for(Publisher publisher : publishers){

                PreparedStatement preparedStatement = connection.prepareStatement("select id, title, publisher_id from book where publisher_id=?");
                preparedStatement.setInt(1, publisher.getId());
                rs = preparedStatement.executeQuery();
                while(rs.next())
                {
                    int id = rs.getInt("id");
                    String title = rs.getString("title");
                    int pid = rs.getInt("publisher_id");
                    publisher.withBook(new Book(id, title, pid));
                }

            }

            System.out.println("publishers = " + publishers);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try(Connection connection = getConnection()){

            connection.setAutoCommit(false);

            String name = "SOFTIQ2 PRESS";
            String logo = "https://softiq.pl/wp-content/themes/softiq//dist/img/logo.png";
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO PUBLISHER(NAME, LOGOIMAGE) VALUES(?,?)");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, logo);

            int rows = preparedStatement.executeUpdate();
            System.out.println("rows = " + rows);

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }

    public static Connection getConnection(){

        String url = "jdbc:mysql://localhost:3306/softiq";
        String user = "root";
        String password = "mysql";

        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
}
