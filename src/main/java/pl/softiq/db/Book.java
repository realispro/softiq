package pl.softiq.db;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book
{
    private int id;
    private String title;
    private int publisherId;
}
