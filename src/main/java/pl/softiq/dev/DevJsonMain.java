package pl.softiq.dev;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DevJsonMain {

    public static void main(String[] args) throws JsonProcessingException {
        System.out.println("Let's serialize!");

        TeamMember tm = new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(tm);
        System.out.println("json = " + json);

        TeamMember deserialized = mapper.readValue(json, TeamMember.class);

        System.out.println("equals: " + tm.equals(deserialized));


        System.out.println("done.");
    }
}
