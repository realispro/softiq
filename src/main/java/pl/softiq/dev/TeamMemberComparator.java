package pl.softiq.dev;

import java.util.Comparator;

public class TeamMemberComparator implements Comparator<TeamMember> {
    @Override
    public int compare(TeamMember tm1, TeamMember tm2) {
        return tm1.getSkills().size() - tm2.getSkills().size();
    }
}
