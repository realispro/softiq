package pl.softiq.dev;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class DevStreamMain {

    public static void main(String[] args) {

        System.out.println("Let's develop!");
        Team team = constructTeam("A Team");
        List<TeamMember> members = team.getMembers();

        // TODO 1: sort by skills count
        List<TeamMember> sortedMembers = members
                .stream()
                .sorted(Comparator.comparingInt(member -> member.getSkills().size()))
                .collect(Collectors.toList());
        //.toList();
        System.out.println("sortedMembers = " + sortedMembers);

        // TODO 2: remove technical writer
        System.out.println("no writers:");
        Consumer<TeamMember> teamMemberConsumer = System.out::println;
        //m -> System.out.println(m);

        members.stream()
                .filter(m -> !m.getRoles().contains(Role.TECHNICAL_WRITER))
                .forEach(teamMemberConsumer);

        // TODO 3: find all java developers
        List<TeamMember> javaDevs =
                members.stream()
                        .distinct()
                        .filter(member -> member.getSkills().contains(Skill.JAVA))
                        .collect(Collectors.toList());
        System.out.println("javaDevs: " + javaDevs);

        // TODO 4: count all javascript developers
        long counter = members.stream()
                .filter(tm -> tm.getSkills().contains(Skill.JAVASCRIPT))
                .count();

        System.out.println("javascript devs count = " + counter);

        System.out.println("done.");
    }

    public static Team constructTeam(String name) {
        Team team = new Team(name);

        TeamMember joe = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        TeamMember joeBis = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        team.addMember(joeBis);

        team.addMember(new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER));

        team.addMember(new TeamMember("Bob")
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.REACT_JS)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.QA));

        team.addMember(new TeamMember("Betty")
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withRole(Role.QA));

        team.addMember(joe);

        team.addMember(new TeamMember("Billy")
                .withSkill(Skill.SQL)
                .withRole(Role.TECHNICAL_WRITER)
                .withRole(Role.PRODUCT_OWNER));

        return team;
    }
}
