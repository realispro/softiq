package pl.softiq.dev;

import java.util.*;

public class DevelopmentMain {

    public static void main(String[] args) {

        System.out.println("Let's develop!");
        Team team = constructTeam("A Team");
        List<TeamMember> members = team.getMembers();

        // TODO 1: sort by skills count

        List<TeamMember> sortedMembers = new ArrayList<>(members);
        Collections.sort(sortedMembers, new TeamMemberComparator());
        System.out.println("sortedMembers = " + sortedMembers);
        for(TeamMember member : sortedMembers)
            System.out.println("Member: " + member.getName() + " (" + member.getSkills().size() + ")");


        // TODO 2: remove technical writer
        Iterator<TeamMember> itr = members.iterator();
        while(itr.hasNext()) {
            TeamMember member = itr.next();
            if(member.getRoles().contains(Role.TECHNICAL_WRITER)){
                itr.remove();
            }
        }

        System.out.println("no writers = " + members);

        // TODO 3: find all java developers
        List<TeamMember> javaDevs = new ArrayList<>();
        for(TeamMember member : members) {
            if(member.getSkills().contains(Skill.JAVA)) {
                javaDevs.add(member);
            }
        }
        System.out.println("javaDevs: " + javaDevs);

        // TODO 4: count all javascript developers
        long counter = 0;
        for (TeamMember teamMember : members) {
            if (teamMember.getSkills().contains(Skill.JAVASCRIPT)) {
                counter++;
            }
        }
        System.out.println("javaScript devs count = " + counter);

        System.out.println("done.");
    }

    public static Team constructTeam(String name) {
        Team team = new Team(name);

        TeamMember joe = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        TeamMember joeBis = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        team.addMember(joeBis);

        team.addMember(new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER));

        team.addMember(new TeamMember("Bob")
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.REACT_JS)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.QA));

        team.addMember(new TeamMember("Betty")
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withRole(Role.QA));

        team.addMember(joe);

        team.addMember(new TeamMember("Billy")
                .withSkill(Skill.SQL)
                .withRole(Role.TECHNICAL_WRITER)
                .withRole(Role.PRODUCT_OWNER));

        return team;
    }
}
