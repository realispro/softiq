package pl.softiq.zoo;


import pl.softiq.zoo.animals.Eagle;
import pl.softiq.zoo.animals.Kiwi;
import pl.softiq.zoo.animals.Shark;
import pl.softiq.zoo.animals.Tuna;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("Let's visit zoo!");

        Eagle bielik = new Eagle("Bielik", 15);
        Eagle george = new Eagle("George", 13);
        Shark nemo = new Shark("Nemo", 199);
        Shark willy = new Shark("Willy", 299);
        Kiwi joe = new Kiwi("Joe", 5);
        Tuna jack = new Tuna("Jack", 50);

        // *** arrays ***
        Animal[] animals = new Animal[6];
        animals[0] = bielik;
        animals[1] = george;
        animals[2] = nemo;

        for(int i=0; i<animals.length; i++){
            System.out.println("animal[" + i + "]=" + animals[i]);
        }

        for( Animal animal : animals){
            System.out.println("animal=" + animal);
        }

        // *** collection ***
        System.out.println("Java Collection Framework");

        //Collection<Animal> animalsCollection = new HashSet<>();
        List<Animal> animalsCollection = new ArrayList<>();
        animalsCollection.add(bielik);
        animalsCollection.add(george);
        animalsCollection.add(nemo);
        animalsCollection.add(willy);
        animalsCollection.add(joe);
        animalsCollection.add(jack);
        animalsCollection.add(jack);

        // *** filtering ***
        /*Iterator<Animal> itr = animalsCollection.iterator();
        while(itr.hasNext()){
            Animal animal = itr.next();
            if(animal.getName().equals("Nemo")){
                itr.remove();
            }
        }*/

        //Predicate<Animal> animalPredicate = a -> a.getName().equals("Nemo");
        animalsCollection.removeIf(a -> a.getName().equals("Nemo"));

        // *** verification ***
        if(!animalsCollection.contains(new Shark("Nemo", 199))){
            System.out.println("Where is Nemo?");
        } else {
            System.out.println("Nemo is still here!");
        }

        System.out.println("animal collection size: " + animalsCollection.size());

        // *** sorting ***
        Comparator<Animal> animalComparator =
                (a1, a2) -> a1.getName().length() - a2.getName().length();
                /*new Comparator<Animal>() {
                    @Override
                    public int compare(Animal a1, Animal a2) {
                        return a1.getName().length() - a2.getName().length();
                    }
                };*/
                //new AnimalByNameComparator();
        Collections.sort(animalsCollection, (a1, a2) -> a1.getName().length() - a2.getName().length());

        // *** reviewing ***
        /*for( Animal animal : animalsCollection){
            System.out.println("animal = " + animal);
        }*/
        //Consumer<Animal> animalConsumer = a -> System.out.println("animal = " + a);
        animalsCollection.forEach(a -> System.out.println("animal = " + a));

        System.out.println("first animal: " + animalsCollection.get(0));

        System.out.println("done.");

    }
}
