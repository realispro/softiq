package pl.softiq.zoo;

import pl.softiq.zoo.animals.Eagle;
import pl.softiq.zoo.animals.Kiwi;
import pl.softiq.zoo.animals.Shark;
import pl.softiq.zoo.animals.Tuna;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZooStreamMain {

    public static void main(String[] args) {

        System.out.println("Let's visit zoo!");

        Eagle bielik = new Eagle("Bielik", 15);
        Eagle george = new Eagle("George", 13);
        Shark nemo = new Shark("Nemo", 199);
        Shark willy = new Shark("Willy", 299);
        Kiwi joe = new Kiwi("Joe", 5);
        var jack = new Tuna("Jack", 50);

        List<Animal> animals = new ArrayList<>();
        animals.add(bielik);
        animals.add(george);
        animals.add(nemo);
        animals.add(willy);
        animals.add(joe);
        animals.add(willy);
        animals.add(jack);

        
        Stream<Animal> stream = animals.stream();
        
        var result = stream

                // *** intermediate ops ***
                .distinct()
                .filter( a -> !a.getName().equals("Nemo"))
                .sorted( (a1,a2)->a1.getMass()-a2.getMass())
                .map( a -> a.getName())
                .peek( n -> System.out.println("name=" + n))

                // *** terminal ops ***
                //.count();
                //.collect(Collectors.toList());
                //.anyMatch(a -> a.getName().contains("k"));
                .findFirst();

        //Supplier<RuntimeException> exceptionSupplier = () -> new RuntimeException("missing animal!");
        System.out.println("result = " + result.orElseThrow(() -> new RuntimeException("missing animal!")));

        IntStream integerStream = IntStream.of(1,2,3,4,5);
                //Arrays.stream(new Integer[]{1,2,3,4,5});
        
        Stream<String> stringStream = Stream.generate(()->"march");
        
        stringStream
                .limit(10)
                .forEach(s-> System.out.println("s = " + s));
        
        


    }
}
