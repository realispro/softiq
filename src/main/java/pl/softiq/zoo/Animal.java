package pl.softiq.zoo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class Animal implements Comparable<Animal>{

    private String name;
    private int mass;

    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public abstract void move();

    @Override
    public int compareTo(Animal a) {
        System.out.println("comparing " + this + " and " + a);
        return a.mass - this.mass;
    }
}
