package pl.softiq.zoo.animals;


import pl.softiq.zoo.Fish;

public class Tuna extends Fish {
    public Tuna(String name, int mass) {
        super(name, mass);
    }
}
