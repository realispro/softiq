package pl.softiq.zoo.animals;


import pl.softiq.zoo.Fish;

public class Shark extends Fish {
    public Shark(String name, int mass) {
        super(name, mass);
    }
}
