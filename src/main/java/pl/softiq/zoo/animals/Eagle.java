package pl.softiq.zoo.animals;


import pl.softiq.zoo.Bird;

public class Eagle extends Bird {
    public Eagle(String name, int mass) {
        super(name, mass);
    }
}
