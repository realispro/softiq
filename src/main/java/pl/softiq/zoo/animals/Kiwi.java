package pl.softiq.zoo.animals;


import pl.softiq.zoo.Bird;

public class Kiwi extends Bird {
    public Kiwi(String name, int mass) {
        super(name, mass);
    }

    @Override
    public void move() {
        System.out.println(this + " is walking");
    }
}
