package pl.softiq.file;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileMain {

    public static void main(String[] args) {
        System.out.println("Let's access file!");

        File dir = new File("mydir");

        if(dir.exists()){
            System.out.println("directory already exists.");
        } else {
            System.out.println("directory is missing. creating...");
            dir.mkdirs();
        }

        File file = new File(dir, "myfile.txt");
        if(file.exists()){
            System.out.println("File already exists.");
        } else {
            System.out.println("File is missing. creating...");
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));){ // try-with-resources

            bw.write(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + "\n");

        } catch (IOException e) {
            throw new RuntimeException(e);
        } /*finally {
            try {
                bw.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/

        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            /*String line;
            while( (line=br.readLine()) !=null  ){
                System.out.println("line:" + line);
            }*/

            br.lines()
                    .forEach(line -> System.out.println("line:" + line));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        System.out.println("done.");
    }
}
